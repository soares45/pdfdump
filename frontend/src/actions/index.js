import axios from 'axios';

export const DATA_FETCH = 'data_fetch';
export const DATA_DELETE = 'data_delete';
export const SET_FETCH = 'set_fetch';
export const SET_NOT_FETCH = 'set_not_fetch';
export const FETCH_LOGIN = 'fetch_login';
export const FETCH_LOGOUT = 'fetch_logout';


export const ROOT_URL = 'http://localhost:80/files';

export function fetchData(){
  const request = axios.get(`${ROOT_URL}/home`);

  return {
    type: DATA_FETCH,
    payload: request
  }
}

export function deleteData(filename, callback){
  const request = axios.get(`${ROOT_URL}/delete/${filename}`);
  request.then(() => callback());
  return {
    type: DATA_DELETE,
    payload: request
  }
}

export function loggin(values, callback){
  const request = axios.post(`${ROOT_URL}/logIn`, values);
  request.then((response) => {
    if(response.data.ERROR){
      console.log(response);
      return 0;
    }
    callback();
  });
  return {
    type: FETCH_LOGIN,
    payload: request
  }
}

export function logOut(callback){
  callback();
  return {
    type: FETCH_LOGOUT,
    payload: {}
  }
}

export function setFetch(){
  return {
    type: SET_FETCH,
    payload: true
  }
}

export function setNotFetch(){
  return {
    type: SET_NOT_FETCH,
    payload: false
  }
}
