import Datatable from './sub_components/datatable';
import { withRouter } from 'react-router-dom';
import Header from './sub_components/Header';
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { loggin } from '../actions';
import swal from 'sweetalert2'

class Accueil extends Component {

	connectionClick(){
		const { session } = this.props;
    if (session && session.adminId) {
			this.props.history.push('/admin');
		} else {
			swal({
				title: 'Connectez-vous!',
				type: 'question',
				html:
		 			'<input type="text" id="swal-input1" class="swal2-input" placeholder="Enter votre username">' +
		 			'<input type="password" id="swal-input2" class="swal2-input" placeholder="Enter votre mot de passe">',
				showCancelButton: true,
				confirmButtonText: 'Ce connecter',
				cancelButtonText: 'Annuler',
				preConfirm: function () {
	    		return [
	    			document.getElementById('swal-input1').value,
	    			document.getElementById('swal-input2').value
	    		]
	  		}
			}).then((value) => {
				if(value && value.value && value.value[0] !== '' && value.value[1] !== ''){
					const info = {};
		     	info.username = value.value[0];
		     	info.password = value.value[1];
					this.props.loggin(info, () => {
						this.props.history.push('/admin');
					});
				}
			}, function(dismiss) {
				// dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
				if (dismiss === 'cancel') {
				}
			});
		}
	}

	render(){
		const { session } = this.props;
		return (
			<div>
				<Header />
				<h2>
					ACCUEIL
				</h2>
				<Datatable />
				<br /><br /><hr />
				<button className="btn btn-primary waves-effect" onClick={(e) => this.connectionClick()}>{session && session.adminId ? "Aller à l'accueil admin" : "Ce connecter"}</button>
			</div>
		);
	}
}

function mapStateToProps({session}){
  return {session};
}

export default withRouter(connect(mapStateToProps, { loggin } )(Accueil));
