import DatatableAdmin from './sub_components/datatableAdmin';
import Header from './sub_components/Header';
import React, {Component} from 'react';
import { Link} from 'react-router-dom';
import { connect } from 'react-redux';
import { logOut } from '../actions';

class AccueilAdmin extends Component {

	deconnectionClick(){
		this.props.logOut(() => {
			this.props.history.push('/');
		});
	}

	render(){
		return (
			<div>
				<Header />
				<h2>
					ACCUEILADMIN
				</h2>
				<DatatableAdmin />
				<br /><br /><hr />
        <button className="btn btn-primary waves-effect" onClick={(e) => this.deconnectionClick()}>Ce deconnecter</button>
        <Link to={`/accueil`}><button className="btn btn-primary waves-effect">Retour &agrave; l&acute;accueil</button></Link>
				<Link to={`/insertion`}><button className="btn btn-primary waves-effect">Ajouter un nouveau document</button></Link>
			</div>
		);
	}
}
export default connect(null, {logOut})(AccueilAdmin);
