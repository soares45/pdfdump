import React, {Component } from 'react';
import DataTables from 'material-ui-datatables';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import {fetchData, ROOT_URL} from '../../actions';
import { connect } from 'react-redux';
import _ from 'lodash';
import FileDownload from 'material-ui-icons/FileDownload';

const TABLE_COLUMNS = [
  {
    key: 'nomDocument',
    label: 'Document',
    sortable: true
  }, {
    key: 'auteur',
    label: 'Auteur',
    sortable: true
  }, {
    key: 'size',
    label: 'Taille',
    sortable: true
  }, {
    key: 'filename',
    label: 'Téléchargement',
    render: (filename, all) => <a download={`${all.nomDocument}`} type={`${all.type}`} href={`${ROOT_URL}/${filename}`}><FloatingActionButton mini={true} className="btn-floating" ><FileDownload /></FloatingActionButton></a>

  }
];

class Datatable extends Component {

  componentDidMount(){
    this.props.fetchData();

  }

  handleSortOrderChange = (key, order) => {
    this.order = order;
    this.key = key;
    this.dataToShow = _.orderBy((this.dataToShow == null ? this.data : this.dataToShow), key,order);
    this.forceUpdate();
  }

  handleFilterValueChange = (filterValue) => {
    if(filterValue !== ''){
      this.dataToShow = [];
      this.data.forEach(d => (d.nomDocument.indexOf(filterValue) !== -1) || (d.auteur.indexOf(filterValue) !== -1) || ((d.size+'').indexOf(filterValue) !== -1) ? this.dataToShow.push(d) : null);
    }else{
      this.dataToShow = null;
    }
    this.handleSortOrderChange(this.key, this.order);
    this.forceUpdate();
  }

  render() {
    this.data = _.values(this.props.table_data);
    var DATA = (this.dataToShow == null ? this.data : this.dataToShow);
    return (
      <MuiThemeProvider>
        <DataTables
          height={'auto'}
          selectable={false}
          showRowHover={true}
          columns={TABLE_COLUMNS}
          data={DATA}
          showCheckboxes={false}
          onFilterValueChange={this.handleFilterValueChange}
          onSortOrderChange={this.handleSortOrderChange}
          showHeaderToolbar={true}
          headerToolbarMode={"filter"}
          onRowSizeChange={this.handleRowSizeChange}
          count={_.keys(DATA).length}
          filterHintText={"Recherche"}
          showFooterToolbar={false}
        />
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps({ table_data }){
  return {table_data} ;
}

export default connect(mapStateToProps, { fetchData } )(Datatable);
