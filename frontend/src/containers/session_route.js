import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch} from 'react-router-dom';
import Insertion from '../components/Insertion';
import Accueil from '../components/Accueil';
import AccueilAdmin from '../components/AccueilAdmin';

class SessionRoute extends Component {

  render() {
    const { session } = this.props;
    if (session && session.adminId) {
      return (
        <Switch>
          <Route exact path="/insertion" component={Insertion} />
          <Route exact path="/admin" component={AccueilAdmin} />
          <Route exact path="/accueil" component={Accueil} />
          <Route exact path="/" component={Accueil} />
        </Switch>
      );
    } else {
      return (
        <Switch>
          <Route exact path="/accueil" component={Accueil} />
          <Route exact path="/" component={Accueil} />
        </Switch>
      );
    }
  }
}

function mapStateToProps({session}){
  return {session};
}

export default connect(mapStateToProps, null)(SessionRoute);
