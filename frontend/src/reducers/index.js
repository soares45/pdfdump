import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import DataReducer from './reducer_table_data';
import Session from './reducer_session';
import IsFetch from './reducer_is_fetch';

const rootReducer = combineReducers({
  form: formReducer,
  table_data: DataReducer,
  isFetch: IsFetch,
  session: Session
});

export default rootReducer;
