import {SET_FETCH, SET_NOT_FETCH} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case SET_FETCH:
      return true;
    case SET_NOT_FETCH:
      return true;
    default:
      return false;
  }
}
