import {FETCH_LOGIN, FETCH_LOGOUT} from '../actions';

export default function(state = {}, action) {
  switch (action.type) {
    case FETCH_LOGIN:
      return action.payload.data;
    case FETCH_LOGOUT:
      return action.payload;
    default:
      return state;
  }
}
