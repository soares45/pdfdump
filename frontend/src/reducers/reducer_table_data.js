import {DATA_FETCH, DATA_DELETE} from '../actions';
import _ from 'lodash';

export default function(state = {}, action) {
  switch (action.type) {
    case DATA_FETCH:
      return _.mapKeys(action.payload.data, "filename");
    case DATA_DELETE:
      return state;
    default:
      return state;
  }
}
