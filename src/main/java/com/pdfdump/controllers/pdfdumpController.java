package com.pdfdump.controllers;

import com.mongodb.gridfs.GridFSDBFile;
import com.pdfdump.domain.Fichier;
import com.pdfdump.domain.Password;
import com.pdfdump.services.FichierService;
import com.pdfdump.services.PasswordService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/files")
public class pdfdumpController {

    @Autowired
    private FichierService fichierService;

    @Autowired
    private PasswordService passwordService;

    @RequestMapping(value = "/home", method = { RequestMethod.GET, RequestMethod.POST })
    public String home(@RequestParam Map<String, String> params) {
        return fichierService.getAll();
    }

    @PostMapping("/java")
    public HttpEntity<byte[]> createOrUpdate(@RequestParam("file") MultipartFile file,
            @RequestParam("document") String document, @RequestParam("auteur") String auteur) {
        fichierService.addFichier(document, auteur, file);
        String resp = "<script>window.location = '/';</script>";
        return new HttpEntity<>(resp.getBytes());
    }

    @PostMapping("/rest")
    public HttpEntity<byte[]> createOrUpdate(@RequestParam("file") MultipartFile file,
            @RequestParam("document") String document, @RequestParam("auteur") String auteur,
            @RequestParam("extension") String extension) {
        fichierService.addFichier(document, auteur, file, extension);
        String resp = "<script>window.location = '/';</script>";
        return new HttpEntity<>(resp.getBytes());
    }

    @RequestMapping(value = "/admins", method = { RequestMethod.GET, RequestMethod.POST })
    public String getAdmins(@RequestParam Map<String, String> params) {
        return passwordService.getAllAdmin();
    }

    @PostMapping("/logIn")
    public String logIn(@RequestBody Password password) {
        return passwordService.logIn(password.getUsername(), password.getPassword());
    }

    @GetMapping
    public @ResponseBody List<String> list() {
        return fichierService.getFiles().stream().map(GridFSDBFile::getFilename).collect(Collectors.toList());
    }

    @GetMapping("/delete/{fileName:.+}")
    public @ResponseBody String delete(@PathVariable String fileName) {
        return fichierService.deleteFile(fileName);
    }

    @GetMapping("/{name:.+}")
    public HttpEntity<byte[]> get(@PathVariable("name") String name) {
        try {
            Optional<GridFSDBFile> optionalCreated = fichierService.maybeLoadFile(name);
            if (optionalCreated.isPresent()) {
                GridFSDBFile created = optionalCreated.get();
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                created.writeTo(os);
                Fichier file = fichierService.getFichierByHex(name);
                String filename = file.getNomDocument()+file.getExtension();
                HttpHeaders headers = new HttpHeaders();
                headers.add(HttpHeaders.CONTENT_TYPE, created.getContentType());
                headers.add("Content-disposition", "attachment; filename="+filename);
                return new HttpEntity<>(os.toByteArray(), headers);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.IM_USED);
        }
    }

}
