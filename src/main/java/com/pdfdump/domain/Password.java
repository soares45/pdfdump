package com.pdfdump.domain;

import java.security.MessageDigest;
import java.util.Base64;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Password extends Admin{

	private String password;

	public Password() {
        super();
    }
	
	public Password(String username,String password) {
		super(username);
		this.password = password;
	}

	public static String sha256(String password) {
		String result = "";
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] passwordBytes = password.getBytes("UTF-8");
			byte[] passwordBytesArray = digest.digest(passwordBytes);
			result = Base64.getEncoder().encodeToString(passwordBytesArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
