package com.pdfdump.exemple;

import java.io.IOException;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;

public class GridFsReadDemo {
    public static void main(String[] args) throws IOException {
        MongoClient mongo = new MongoClient("192.168.99.100", 32768);
        DB db = mongo.getDB("filesdb");

        // Create instance of GridFS implementation
        GridFS gridFs = new GridFS(db);

        // Find the image with the name image1 using GridFS API
        GridFSDBFile outputFile = gridFs.findOne("ISO");

        // Get the number of chunks
        System.out.println("Total Chunks: " + outputFile.numChunks());

        // Location of the image read from MongoDB to be written
        String location = "D:/ISO2.zip";
        outputFile.writeTo(location);
        mongo.close();

    }
}
