package com.pdfdump.exemple;

import java.io.File;
import java.io.IOException;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;

public class GridFsSaveDemo {

    public static void main(String[] args) throws IOException {
        MongoClient mongo = new MongoClient("192.168.99.100", 32768);
        DB db = mongo.getDB("filesdb");

        //Location of file to be saved
        String imageLocation = "D:/ISO.zip";

        //Create instance of GridFS implementation
        GridFS gridFs = new GridFS(db);

        //Create a file entry for the image file
        GridFSInputFile gridFsInputFile = gridFs.createFile(new File(imageLocation));

        //Set a name on GridFS entry
        gridFsInputFile.setFilename("ISO");

        //Save the file to MongoDB
        gridFsInputFile.save();
      }
}
