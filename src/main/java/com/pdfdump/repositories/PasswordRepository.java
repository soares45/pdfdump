package com.pdfdump.repositories;

import com.pdfdump.domain.Password;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PasswordRepository extends MongoRepository<Password, ObjectId>{

    List<Password> findAll();
    Password findByUsername(String username);
}
