package com.pdfdump.services;

import com.google.gson.Gson;
import com.pdfdump.domain.Admin;
import com.pdfdump.domain.Password;
import com.pdfdump.repositories.PasswordRepository;
import com.pdfdump.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class PasswordService extends Service {

    @Autowired
    PasswordRepository passwordRepository;

    PasswordService(){}

    public String getAllAdmin(){
        List<Admin> admins = new ArrayList<>();
        for (Password password: passwordRepository.findAll()) {
            admins.add(password);
        }
        return new Gson().toJson(admins);
    }


    public String addPassword(String username, String password){
        if (username == null || password == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Password not registered");
        Password newPassword = new Password(username, Password.sha256(password));
        passwordRepository.save(newPassword);
        return Utils.jsonReply("ERROR", false, "MESSAGE", "Password registered successfully");
    }

    public String logIn(String username, String password){
        if(username == null || password == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "Username or password null");
        Password account = passwordRepository.findByUsername(username);
        if (account == null)
            return Utils.jsonReply("ERROR", true, "MESSAGE", "No such username", "USERNAME", username );
        if (account.getPassword().equals(Password.sha256(password)))
            return Utils.jsonReply("ERROR", false, "MESSAGE", "Welcome home", "adminId", account.getId().toHexString
                    (), "admin", account.getUsername());
        return Utils.jsonReply("ERROR", true, "MESSAGE", "Wrong password", "USERNAME", username);
    }
}
